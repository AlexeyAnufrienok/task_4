<?php

class ControllerExtensionModuleProductbarcode extends Controller {
    private $error = array();

    public function index() {
        // Подгружаем языковой файл
        $this->load->language('extension/module/productbarcode');

        // С языкового файла берем заголовок и устанавливаем его в качестве заголовка модуля
        $this->document->setTitle($this->language->get('heading_title'));

        // Загружаем стандартную модель для вызова метода сохранения настроек (можно использовать свою?)
        // $this->load->model('extension/module/productbarcode'); - например, своя модель у модуля
        $this->load->model('setting/setting');

        // Сохранение настроек модуля, когда пользователь нажал "Записать" - проверяем метод запроса должен быть POST и должна быть пройдена валидация (функция валидации предоставлена ниже)
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            // Вызываем метод модели для сохранения настроек
            $this->model_setting_setting->editSetting('module_productbarcode', $this->request->post);
            // $this->model_extension_module_productbarcode->SaveSettings(); - если бы мы использовали не стандартную модель а свою модель модуля

            // Выходим из настроек с выводом сообщения
            $this->session->data['success'] = $this->language->get('text_success');

            // Делаем редирект на страницу выбора модулей в "расширениях"
            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        // Блок ошибок
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        // Блок хлебных крошек - путь вверху модуля, по которому можно переместиться на страницы уровнем выше
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/productbarcode', 'user_token=' . $this->session->data['user_token'], true)
        );

        // Добавляем в массив $data ссылку действия с ключем action
        $data['action'] = $this->url->link('extension/module/productbarcode', 'user_token=' . $this->session->data['user_token'], true);

        // Добавляем в массив $data ссылку отмены с ключем cancel
        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        // Добавляем в массив $data значение статуса, если он был в запросе, если нет - то берем с БД
        if (isset($this->request->post['module_productbarcode_status'])) {
            $data['module_productbarcode_status'] = $this->request->post['module_productbarcode_status'];
        } else {
            $data['module_productbarcode_status'] = $this->config->get('module_productbarcode_status');
        }

        // Добавляем в массив $data общий вид админ-панели
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Отправляем ответ в представление модуля
        $this->response->setOutput($this->load->view('extension/module/productbarcode', $data));
    }

    // Функция валидации (здесь проверка на доступ. Например, если есть форма сюда можно добавлять проверку длину названий, диапазон чисел и пр. => передавать в $data и в дальнейшем отображать в представлении.
    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/productbarcode')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    // Функция установки - выполняется один раз, когда происходит активация модуля
    public function install() {
        // Подгружаем модели событий
        $this->load->model('setting/event');

        // Вызываем функцию addEvent(имя модуля, триггер, событие) из модели событий, которая в свою очередь создает (записывает в БД). Триггер - условие при котором будет выполнятся событие. Событие - функция, которую нужно выполнить
//        $this->model_setting_event->addEvent('productbarcode', 'admin/model/catalog/product/editProduct/after', 'extension/module/productbarcode/createpdf');
        $this->model_setting_event->addEvent('productbarcode', 'admin/model/extension/productBarcode/productBarcode/createPdf/after', 'extension/module/productbarcode/createpdf');
    }

    // Функция удаления  - выполняется один раз, когда происходит деактивация модуля
    public function uninstall() {
        // Подгружаем модели событий
        $this->load->model('setting/event');

        // Вызываем функцию deleteEventByCode(имя модуля) из модели событий, которая в свою очередь удаляет событие из БД по имени модуля
        $this->model_setting_event->deleteEventByCode('productbarcode');
    }
}