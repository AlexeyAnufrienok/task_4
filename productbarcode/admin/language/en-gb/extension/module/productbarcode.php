<?php
// Headers
$_['heading_title']    = 'Product Barcode';

// Text
$_['text_edit']        = 'Edit Product Barcode Module';
$_['text_extension']   = 'Extensions';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Product Barcode module!';
