<?php
// Заголовки
$_['heading_title']    = 'Product Barcode';

// Текст
$_['text_edit']        = 'Редактировать модуль Product Barcode';
$_['text_extension']   = 'Расширения';

// Ввод
$_['entry_status']     = 'Статус';

// Ошибка
$_['error_permission'] = 'У Вас не хватает прав для изменение модуля Product Barcode!';
